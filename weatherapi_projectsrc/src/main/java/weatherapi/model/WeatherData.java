package weatherapi.model;

import java.util.List;

public class WeatherData {

	private Location location;
	private List<Observation> observations;
	
	public WeatherData() {}
	
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}


	public List<Observation> getObservations() {
		return observations;
	}


	public void setObservations(List<Observation> observations) {
		this.observations = observations;
	}
}
