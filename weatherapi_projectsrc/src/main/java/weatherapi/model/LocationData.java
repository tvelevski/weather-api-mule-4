package weatherapi.model;

import java.util.List;

public class LocationData {

	
	private List<Location> location;

	public LocationData() {}
	
	public List<Location> getLocation() {
		return location;
	}

	public void setLocation(List<Location> location) {
		this.location = location;
	}
}
