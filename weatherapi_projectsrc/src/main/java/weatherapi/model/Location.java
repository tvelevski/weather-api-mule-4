package weatherapi.model;

public class Location {

	private String country;
	private String city;
	
	public Location() {}
	
	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCountry() {
		return country;
	}
}
